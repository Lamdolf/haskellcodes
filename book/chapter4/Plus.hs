a `plus` b = a + b

data a `Pair` b = a `Pair` b
                  deriving (Show)


-- Infix is purely a syntatic convenience
-- the constructor can be used as infix or prefix
foo = Pair 1 2 
bar = True `Pair` "aoeurtoanu"
