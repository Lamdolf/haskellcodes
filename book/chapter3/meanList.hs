-- myLength calcula el largo de la lista entregada
myLength :: [a] -> Int
myLength (x:xs) = 1 + myLength xs
myLength [] = 0


-- mySum calcula la suma de todos los numeros de la lista entregada
mySum :: [Int] -> Int
mySum (x:xs) = x + mySum xs
mySum [] = 0


-- MeanList calcula el promedio de una lista de numeros, basicamente divide
-- la suma de todos, por la cantidad de numeros
meanList :: [Int] -> Double
meanList xs = listSum / listLength 
              where
                  listSum = fromIntegral (mySum xs) :: Double
                  listLength = fromIntegral (myLength xs) :: Double
