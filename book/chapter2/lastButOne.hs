lastButOne :: [a] -> a
lastButOne xs = if length xs >= 2 then
                   last (take (length xs - 1) xs)
                else
                   last xs

