nose :: Integer -> Maybe Integer
nose a
    | a < 20 = Just 20
    | otherwise = Nothing 
