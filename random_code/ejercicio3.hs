polinome :: Integer -> Integer
polinome x = x^3 - 2*x^2 + 3*x - 5

increment :: Integer -> Integer
increment n
  | n > 10 = n
  | n < (-10) = n*(-1)
  | otherwise = 0

main = do
    print(increment (polinome 5))
