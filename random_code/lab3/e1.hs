absolute :: Integer -> Integer
absolute n = if n < 0 then
                n * (-1)
             else
                n

main = do
    print "Ingrese la distancia de lanzamiento:"
    distance <- getLine
    print "La distancia de lanzamiento (absoluta) es"
    print(absolute (read distance::Integer))
