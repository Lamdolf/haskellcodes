polinome :: Integer -> Integer
polinome x = x^3 - 2 * x^2 + 3 * x - 5

increment :: Integer -> (String, Integer)
increment n
  | n < (-10) = ("El precio baja por ", abs n)
  | n > 10 = ("El precio sube por ", n)
  | otherwise = ("El precio se mantiene, por lo que su incremento es ", 0)


main = do
    print "Ingrese valor del gobierno"
    x <- getLine
    print(increment (polinome (read x::Integer)))
