winner :: Integer -> Integer -> String
winner puntaje1 puntaje2
    | puntaje1 > puntaje2 = "Gana el jugador 1"
    | puntaje2 > puntaje1 = "Gana el jugador 2"
    | otherwise = "Es un empate"


main = do
    print "Ingrese puntaje jugador 1"
    p1 <- getLine
    print "Ingrese puntaje jugador 2"
    p2 <- getLine
    print(winner (read p1::Integer) (read p2::Integer))
