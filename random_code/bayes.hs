disease :: Double
disease = 0.05

positive :: Double
positive = 0.99

nonDisease :: Double
nonDisease = 1 - disease

nonPositive :: Double
nonPositive = 1 - positive

negative :: Double
negative = 0.99

nonNegative :: Double
nonNegative = 1-negative

bayes :: Double
bayes = (disease * positive) / (positive * disease + disease * nonNegative)

